---
layout: post
title:  "Seminario Mariana Vivas"
date:   2021-11-02 00:00:00 -0400
categories: PTEG
---
## Presentación de Proyecto de TEG

* Búsqueda de nueva física utilizando técnicas de aprendizaje automático en eventos de múltiples jets: análisis comparativo de algoritmos de clasificación en términos de reproducibilidad y rendimiento
* por: Mariana Vivas
* Asesoría:
	* José Antonio López (UCV)
	* Reina Camacho

## Jurado

|Principal|Suplente|
|:---:|:---:|
|José Antonio López (Tutor)||
|Joany Manjarrés|Arturo Sánchez|
|Gabriel Abellán|Esteban Álvarez|

## Fecha
2021-11-03 08:00:00 -0400

## Lugar
 <a href="https://meet.jit.si/ref.424.seminario-pteg-mariana-vivas">https://meet.jit.si/ref.424.seminario-pteg-mariana-vivas</a> 

## Poster
[Anuncio](../Anuncios/CI_Mariana_Vivas_Anuncio_Proyecto_TEG.pdf)
