---
layout: post
title:  "TEG Eli Flores"
date:   2023-04-20 00:00:00 -0400
categories: TEG
---
## Presentación de TEG

* Diseño y construcción de un controlador automatizado de temperatura, basado en la plataforma de desarrollo ARDUINO, para un sistema de calentamiento por inducción electromagnética
* por: Eli Flores
* Asesoría: 
	* Humberto Rojas (Tutor UCV)
	* Jimmy Castillo

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Humberto Rojas (Tutor)||
|Delfín Moronta|Esteban Álvarez|
|Heidi Martínez|José Jorge|

## Fecha
2023-04-24 11:00:00 -0400

## Lugar
Sala de seminarios del Centro de Computación, Facultad de Ciencias, UCV


