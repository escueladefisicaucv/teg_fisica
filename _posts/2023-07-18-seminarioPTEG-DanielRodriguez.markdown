---
layout: post
title:  "Seminario Daniel Alfredo Rodríguez De Sousa"
date:   2023-07-17 00:00:00 -0400
categories: PTEG
---
## Presentación de Proyecto de TEG

* Arqueología extragaláctica con cúmulos globulares
* por: Daniel Alfredo Rodríguez De Sousa
* Asesoría: 
	* Ernesto Fuenmayor (Tutor UCV)
	* Iván Cabrera Ziri (Universität Heidelberg)

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Ernesto Fuenmayor (Tutor)||
|José Antonio López Rodríguez|Gabriel Abellán|
|Efraín Gatuzz|Cecilia Mateu|

## Fecha
2023-07-18 14:00:00 -0400

## Lugar
Sala de reuniones del Postgrado de la Facultad de Ciencias

Videoconferencia: <a href="https://meet.jit.si/Ref.672.Seminarioptegescueladefisica">https://meet.jit.si/Ref.672.Seminarioptegescueladefisica</a>
