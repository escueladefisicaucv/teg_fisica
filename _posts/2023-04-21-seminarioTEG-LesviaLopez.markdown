---
layout: post
title:  "TEG Lesvia López"
date:   2023-04-12 12:00:00 -0400
categories: TEG
---
## Presentación de TEG

* ELABORACIÓN DE PELÍCULAS METÁLICAS MAGNÉTICAS Y SU CARACTERIZACIÓN MEDIANTE MICROSCOPÍA DE PROXIMIDAD
* por: Lesvia López
* Asesoría: 
	* Carlos Rojas (Tutor UCV)
	* Antonio Monsalve

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Carlos Rojas (Tutor)||
|Joszaira Lárez|Heidi Martínez|
|Ricardo Castell|José Jorge|

## Fecha
2023-04-21 08:00:00 -0400

## Lugar
Centro de Microscopía Electrónica, Facultad de Ciencias, UCV


