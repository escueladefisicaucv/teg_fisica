---
layout: post
title:  "TEG Mariana Vivas"
date:   2022-10-18 00:00:00 -0400
categories: TEG
---
## Presentación de TEG

* Búsqueda de nueva física utilizando técnicas de aprendizaje automático en eventos de múltiples jets: análisis comparativo de algoritmos de clasificación en términos de reproducibilidad y rendimiento
* por: Mariana Vivas
* Asesoría:
	* José Antonio López (UCV)
	* Reina Camacho

## Jurado

|Principal|Suplente|
|:---:|:---:|
|José Antonio López (Tutor)||
|Joany Manjarrés|Arturo Sánchez|
|Gabriel Abellán|Esteban Álvarez|

## Fecha
2022-10-20 07:00:00 -0400

## Lugar
Sala de Reuniones del Postgrado de la Facultad de Ciencias

 <a href="https://meet.jit.si/ref.424.seminario-pteg-mariana-vivas">https://meet.jit.si/ref.424.seminario-pteg-mariana-vivas</a> 
<!--
## Poster
[Anuncio](../Anuncios/Mariana_Vivas_Anuncio_TEG.pdf)
-->
