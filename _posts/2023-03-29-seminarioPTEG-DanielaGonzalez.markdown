---
layout: post
title:  "Seminario Daniela González"
date:   2023-03-29 00:00:00 -0400
categories: PTEG
---
## Presentación de Proyecto de TEG

* Redes Neurales Físicamente Informadas y Problema Inverso
* por: Daniela González
* Asesoría: 
	* Pedro García (Tutor UCV)

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Pedro García (Tutor)||
|Esteban Álvarez|Nuri Hurtado|
|Gabriel Abellán|Gustavo Ramírez|

## Fecha
2023-03-27 15:00:00 -0400

## Lugar
Laboratorio Docente de Computación de la Escuela de Física
