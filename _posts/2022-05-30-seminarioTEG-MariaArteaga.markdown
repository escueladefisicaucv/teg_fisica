---
layout: post
title:  "TEG María Arteaga"
date:   2022-05-24 00:00:00 -0400
categories: TEG
---
## Presentación de TEG

* Estudio perturbativo de interacciones en electrodinámica cuántica en presencia de medios materiales
* por: María Arteaga
* Asesoría:
	* José Antonio López Rodríguez (UCV)

## Jurado

|Principal|Suplente|
|:---:|:---:|
|José Antonio López Rodríguez (Tutor)||
|Mario Caicedo|Eudomar Henrríquez|
|Gabriel Abellán|Ernesto Fuenmayor|

## Fecha
2022-05-30 13:00:00 -0400

## Lugar

<a href="https://meet.jit.si/ref.631.defensa-teg-mar%C3%ADa-arteaga">https://meet.jit.si/ref.631.defensa-teg-mar%C3%ADa-arteaga</a> 


<!--<img src="https://gitlab.com/escueladefisicaucv/teg_fisica/-/raw/master/Anuncios/MariaArteaga_Anuncio_TEG.png" alt="qr enlace a conferencia" width="200"/>
-->

## Poster
[Anuncio](https://gitlab.com/escueladefisicaucv/teg_fisica/-/raw/master/Anuncios/MariaArteaga_Anuncio_TEG.pdf?inline=false)
