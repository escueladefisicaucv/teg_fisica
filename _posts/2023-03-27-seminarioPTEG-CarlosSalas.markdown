---
layout: post
title:  "Seminario Carlos Salas"
date:   2023-03-24 00:00:00 -0400
categories: PTEG
---
## Presentación de Proyecto de TEG

* Estudio computacional de la cinética de sustitución de nitrógeno/oxígeno en aleaciones Ti1-x-yAlOxNy
* por: Carlos Salas
* Asesoría: 
	* Hely Cordero (Tutor UCV)
	* Vladimiro Mujica

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Hely Cordero (Tutor)||
|Rosa Mujica|Esteban Álvarez|
|Pierre Embaid|Heidi Martínez|

## Fecha
2023-03-27 15:00:00 -0400

## Lugar
Sala de seminarios del Centro de Computación

Y  <a href="https://meet.jit.si/SeminarioCarlosSalas">https://meet.jit.si/SeminarioCarlosSalas</a> 
