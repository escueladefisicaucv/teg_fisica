---
layout: post
title:  "TEG Jhoalys Valera"
date:   2022-05-23 00:00:00 -0400
categories: TEG
---
## Presentación de TEG

* Efecto radiobiológico de neutrones en grandes altitudes
* por: Jhoalys Valera
* Asesoría:
	* José Antonio López Rodríguez (UCV)
	* Rafael Martín Landrove (UCV)

## Jurado

|Principal|Suplente|
|:---:|:---:|
|José Antonio López (Tutor)||
|Pierre Embaid|Jesús Dávila|
|Luis Núñez|Haydn Barros|

## Fecha
2022-05-26 16:00:00 -0400

## Lugar

<a href="https://campusvirtualucv.org/ead/mod/jitsi/formuniversal.php?id=343195&c=3306937055&t=1653594062">https://campusvirtualucv.org/ead/mod/jitsi/formuniversal.php?id=343195&c=3306937055&t=1653594062</a>

<!--<img src="https://gitlab.com/escueladefisicaucv/teg_fisica/-/raw/master/Anuncios/JhoalysValera_Anuncio_TEG.png" alt="qr enlace a conferencia" width="200"/>-->


## Poster
[Anuncio](https://gitlab.com/escueladefisicaucv/teg_fisica/-/raw/master/Anuncios/JhoalysValera_Anuncio_TEG.pdf?inline=false)
