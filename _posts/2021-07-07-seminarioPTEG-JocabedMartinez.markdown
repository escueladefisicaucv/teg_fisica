---
layout: post
title:  "Seminario Jocabed Martínez"
date:   2021-07-05 00:00:00 -0400
categories: PTEG
---
## Presentación de Proyecto de TEG

* Espectros de rayos X en alta resolución de la binaria de rayos X de baja masa GX 339-4: el origen de las líneas de absorción
* por: Jocabed Martinez
* Asesoría: Fermín Dalmagro (UCV) y Efrain Gatuzz

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Fermín Dalmagro (Tutor)||
|José Antonio López|Carolina Bessega|
|Rosa Mujica|Esteban Álvarez|

## Fecha
2021-07-07 14:30:00 -0400

## Lugar
https://meet.google.com/ohi-uaec-uxd

## Poster
[Anuncio](../Anuncios/CI_JocabedMartinez_Anuncio_Proyecto_TEG.pdf)
