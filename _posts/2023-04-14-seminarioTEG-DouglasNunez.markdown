---
layout: post
title:  "TEG Douglas Nuñez"
date:   2023-04-12 11:00:00 -0400
categories: TEG
---
## Presentación de TEG

* Estudio del comportamiento eléctrico en hidroxiapatita dopada obtenida a partir de biodesechos
* por: Douglas Nuñez
* Asesoría: Jackeline Quiñones

## Jurado

|Principal|Suplente|
|:---:|:---:|
|Jackeline Quiñones (Tutora)||
|Joszaira Lárez|José Manuel Martínez|
|José Jorge|Humberto Rojas|

## Fecha
2023-04-14 08:00:00 -0400

## Lugar
Aula 34, Facultad de Ciencias, UCV


