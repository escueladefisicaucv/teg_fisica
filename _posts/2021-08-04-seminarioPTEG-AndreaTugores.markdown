---
layout: post
title:  "Seminario Andrea Tugores"
date:   2021-08-03 00:00:00 -0400
categories: PTEG
---
## Presentación de Proyecto de TEG

* Desarrollo de un modelo reproducible para la simulación de dosis de secundarios de radiación cósmica en tripulación de aeronaves basado en GEANT4
* por: Andrea Tugores
* Asesoría: José Antonio López

## Jurado

|Principal|Suplente|
|:---:|:---:|
|José Antonio López (Tutor)||
|Arturo Sanchez|Fermín Dalmagro|
|Miguel Martín|Rafael Martín|

## Fecha
2021-08-04 08:00:00 -0400

## Lugar
 <a href="https://meet.jit.si/ref.336.seminarioandtug2021">https://meet.jit.si/ref.336.seminarioandtug2021</a> 

## Poster
[Anuncio](../Anuncios/CI_AndreaTugores_Anuncio_Proyecto_TEG.pdf)
