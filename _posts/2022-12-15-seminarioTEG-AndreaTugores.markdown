---
layout: post
title:  "TEG Andrea Tugores"
date:   2022-12-08 00:00:00 -0400
categories: TEG
---
## Presentación de TEG

* Desarrollo de un modelo reproducible para la simulación de dosis de secundarios de radiación cósmica en tripulación de aeronaves basado en GEANT4
* por: Andrea Tugores
* Asesoría: José Antonio López

## Jurado

|Principal|Suplente|
|:---:|:---:|
|José Antonio López (Tutor)||
|Arturo Sanchez|Fermín Dalmagro|
|Miguel Martín|Rafael Martín|

## Fecha
2022-12-15 08:00:00 -0400

## Lugar
Sala de Reuniones del Consejo de la Facultad de Ciencias, y
 <a href="https://meet.jit.si/ref.336.seminarioandtug2021">https://meet.jit.si/ref.336.seminarioandtug2021</a> 


## Poster
[Anuncio](../Anuncios/Andrea_Tugores_Anuncio.pdf)

