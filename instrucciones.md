---
layout: page
title: Procedimiento
permalink: /instrucciones/
---
# Proceso Trabajo Especial de Grado
Este repositorio será una guía completa en la procedimiento de propuesta y evaluación de los trabajos especiales de grado de estudiantes en la Licenciatura de Física de la UCV

## Actores relevantes
* Estudiante
* Tutor (UCV). 
	* Profesor ordinario Departamento de Física UCV.
	* Coordinador de la evaluación. 
	* Responsable institucional del progreso del proyecto.
* Tutor (2). 
	* Es una figura excepcional: proyectos externos a la UCV, o proyectos multidisciplinarios. 
* Comisión Académica de la Escuela de Física
* Consejo de Escuela

|Importante a partir de junio 2021|
|:---:|
|Tutor solicita a la Escuela de Física iniciar un repositorio para el proyecto, indicando los nombres de usuario gitlab de tutor y estudiante.|
|Es necesario utilizar una cuenta gitlab para acceder a los contenidos|
|En caso de no tener cuenta: [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up)|

## Normativas
Las normativas están publicadas en la sección [Normativas y Baremos](https://gitlab.com/escueladefisicaucv/teg_fisica/-/tree/master/NormativasYBaremos).

## Fase Proyecto (Seminario)
* Debe existir un anteproyecto claro y concreto. Para efectos operativos de la evaluación se debe completar toda la información solicitada en la planilla de proyecto.
* Llenar la planilla de proyecto es responsabilidad única del tutor.
* El modelo de planilla de proyecto está en la sección [PlanlillaProyecto](https://gitlab.com/escueladefisicaucv/teg_fisica/-/tree/master/PlantillaProyecto) de este repositorio.

### Dirección de la Escuela:
Una vez designados los jurados del seminario, se enviará la siguiente información a tutores, jurados y estudiante:
* Carta de designación de jurados
* Plantilla de anuncio para ser publicado en cartelera
* Proyecto de TEG propuesto
* Normativa de TEG
* Baremos de evaluación
* Modelos de comunicaciones
* Formato de monografía
* Planilla de evaluación de seminario

### Evaluación del seminario:
* Tutor (UCV) (coordinador del jurado)
	* Convocan al jurado y fijan el lugar y fecha de la presentación del seminario. Solicitan acta de notas la DCE, con al menos 3 días de antelación, y deben imprimir el Planilla de evaluación de seminario.
	* El jurado evaluará la exposición del proyecto con base a los parámetros establecidos en la normativa de TEG.
	* Los documentos Acta de notas y Planilla de evaluación de seminario deben ser consignados en la Dirección de la Escuela. La Planilla de evaluación de seminario sellada y escaneada será enviada al tesista y los jurados.
	* Hay ayuda relacionada con los documentos de evaluación en la sección [Baremos](https://gitlab.com/escueladefisicaucv/teg_fisica/-/tree/master/NormativasYBaremos/Baremos)

## Fase TEG
### Tesistas
* Favor tomar en cuenta el formato de monografía aprobado por el Consejo de Escuela: [https://www.overleaf.com/read/fzbcxfbctrqh](https://www.overleaf.com/read/fzbcxfbctrqh)
* Tutores
	* La monografía revisada (versión para el jurado) debe ser consignada en el repositorio del proyecto.

### Dirección de la Escuela
Una vez designados los jurados del TEG, se enviará la siguiente información a tutores, jurados y estudiante:
* Carta de designación de jurados
* Plantilla de anuncio para ser publicado en cartelera
* Proyecto de TEG propuesto
* Monografía a ser revisada
* Normativa de TEG
* Baremos de evaluación
* Modelos de comunicaciones
* Formato de monografía
* Planilla de evaluación de seminario (consignada al evaluar el seminario, digitalizada)

### Evaluación del TEG:
#### Tutor (UCV) (coordinador del jurado)
* Solicitar el acta de notas en la DCE, con al menos 3 días de antelación. Una vez fijados lugar y fecha de la defensa, deben informarlos al Consejo de Escuela para recibir el modelo de veredicto.

* El jurado evaluará el TEG con base a los parámetros establecidos en la normativa.
 * Los documentos Acta de notas y Veredicto deben ser consignados en la Dirección de la Escuela. El veredicto sellado y escaneado será enviado al tesista y los jurados.

### Tesista
* El veredicto digitalizado debe ser embebido en el documento de TEG a ser consignado en la DCE, siguiendo el formato aprobado por el Consejo de Escuela de Física.
